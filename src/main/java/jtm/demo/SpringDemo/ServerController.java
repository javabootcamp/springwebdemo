package jtm.demo.SpringDemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class ServerController {

    @GetMapping("/welcome")
    public String welcome(@RequestParam(required = false, defaultValue = "server") String side) {
        return "Welcome to the " + side + "  side";
    }

    @GetMapping("/bye")
    public String bye(@RequestParam(required = false, defaultValue = "server") String side) {
        return "Bye from to the " + side + "  side";
    }

    @GetMapping("/teacher")
    public List<Teacher> teachersList() {
        Teacher t = new Teacher(50,"John","Doe");
        Teacher t1 = new Teacher(51,"Jane","Doe");
        return Arrays.asList(t,t1);
    }
}
